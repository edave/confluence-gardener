# Confluence Gardener #

The Confluence Gardener is a static Atlassian Connect add-on which helps you maintain your Confluence instance.

## Setup ##

There are number of easily installed prerequisites to run Confluence Gardener:

1. [The Atlassian SDK](https://developer.atlassian.com/display/DOCS/Install+the+Atlassian+SDK+on+a+Linux+or+Mac+System)
1. Confluence running locally in Cloud mode
1. A locally running HTTP server

All commands listed below are run from within the directory you cloned Confluence Gardener to.

### 1. Install the Atlassian SDK ###

The Atlassian Plugin SDK provides you with a toolkit of commands which are vital for developing Atlassian plugins. To run Confluence Gardener, we'll be using a single command, [`atlas-run-standalone`](https://developer.atlassian.com/display/DOCS/atlas-run-standalone), which runs a copy of an Atlassian product on your computer. 

Use the following links to install the SDK on your computer:

 - [Instructions for Windows users](https://developer.atlassian.com/display/DOCS/Install+the+Atlassian+SDK+on+a+Windows+System)

 - [Instructions for Mac/Linux users](https://developer.atlassian.com/display/DOCS/Install+the+Atlassian+SDK+on+a+Linux+or+Mac+System)

### 2. Start Confluence ###

After having installed the Atlassian SDK, run the following command to start Confluence Cloud locally in development mode:

```
#!bash
$ atlas-run-standalone --product confluence --version 5.5-OD-31-007 --bundled-plugins com.atlassian.plugins:atlassian-connect-plugin:1.1.0-rc.4,com.atlassian.jwt:jwt-plugin:1.1.0,com.atlassian.bundles:json-schema-validator-atlassian-bundle:1.0.4,com.atlassian.upm:atlassian-universal-plugin-manager-plugin:2.17.2,com.atlassian.webhooks:atlassian-webhooks-plugin:1.0.6 --jvmargs -Datlassian.upm.on.demand=true
```

This command will take a while to download dependencies the first time it is run. When it completes successfully you will see the following:

```
[INFO] [talledLocalContainer] Tomcat 6.x starting...
[INFO] [stalledLocalDeployer] Deploying [/Users/dave/src/confluence-gardener/amps-standalone/target/confluence/confluence.war] to [/Users/dave/src/confluence-gardener/amps-standalone/target/container/tomcat6x/cargo-confluence-home/webapps]...
[INFO] [talledLocalContainer] Tomcat 6.x started on port [1990]
[INFO] confluence started successfully in 584s at http://localhost:1990/confluence
[INFO] Type Ctrl-D to shutdown gracefully
[INFO] Type Ctrl-C to exit
```

When you see this, you can load `http://localhost:1990/confluence` in your browser. Use `admin` for the username and password. You should see something like the following:

![Screen Shot 2014-08-20 at 17.13.06.png](https://bitbucket.org/repo/4oRzXn/images/488482279-Screen%20Shot%202014-08-20%20at%2017.13.06.png)
### 3. Host your add-on locally ###

To be able to install Confluence Gardener, you need to make it accessible over HTTP so Confluence can load it. The simplest way to do this is with the Python SimpleHTTPServer. To run SimpleHTTPServer on port 8000, run the following command from within the directory you cloned Confluence Gardener:

```
#!bash
$ python -m SimpleHTTPServer 8000
```

You can verify this step by loading `http://localhost:8000/atlassian-connect.json` in your browser. You should see something like this:

![Screen Shot 2014-08-20 at 17.24.40.png](https://bitbucket.org/repo/4oRzXn/images/2657192520-Screen%20Shot%202014-08-20%20at%2017.24.40.png)
### 4. Install Confluence Gardener ###

Installing Confluence Gardener locally is really easy. Simply run the script we provided called `install-confluence-gardener.sh`.

```
#!bash
$ ./install-confluence-gardener.sh
```

Which will produce something like the following output:

```
#!bash
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
{"type":"PLUGIN_INSTALL","pingAfter":100,"status":{"source":"atlassian-connect.json","amountDownloaded":0,"done":false,"contentType":"application/vnd.atl.plugins.install.downloading+json","statusCode":200},"links":{"self":"/confluence/rest/plugins/1.0/pending/92be9bc1-4986-4718-9dcc-3052e36ca0a5"},"timestamp":1408573626107,"userKey":"8b8181c547f577520147f57761860002","id":"92be9bc1-4986-4718-9dcc-3052e36ca0a5"}
```

You can re-run this command at any time, but it is only required if you make a change to the `atlassian-connect.json` descriptor.

### Using Confluence Gardener ###

The link to Confluence Gardener can be found in the `Tools` drop down at the top right of any Page or Blog Post once installed.

In Confluence, load the `Demonstration Space` which has been automatically loaded for you. You can find a link in the Spaces drop-down at the top of the screen:

![Screen Shot 2014-08-20 at 17.38.47.png](https://bitbucket.org/repo/4oRzXn/images/3755321602-Screen%20Shot%202014-08-20%20at%2017.38.47.png)

Once you have loaded the Demonstration Space, click the Tools menu at the top right and choose Confluence Gardener:

![Screen Shot 2014-08-20 at 17.39.29.png](https://bitbucket.org/repo/4oRzXn/images/3216280492-Screen%20Shot%202014-08-20%20at%2017.39.29.png)

Welcome to Confluence Gardener!

![Screen Shot 2014-08-20 at 17.37.03.png](https://bitbucket.org/repo/4oRzXn/images/2625805996-Screen%20Shot%202014-08-20%20at%2017.37.03.png)