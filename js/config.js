require.config({
    baseUrl: "/js",
    paths: {
        "ap-loader": "/js/space-graph/ap-loader",
        "purl": "/lib/purl/purl",
        "vis": "/lib/vis/dist/vis",
        "jquery": "/lib/jquery/dist/jquery",
        "lodash": "/lib/lodash/dist/lodash.compat.min",
        "graph": "space-graph/graph",
        "host": "space-graph/host",
        "message": "space-graph/message",
        "tooltips": "space-graph/tooltips"
    },

    shim: {
        "jquery": {
            exports: "$"
        },

        "lodash": {
            exports: "_"
        }
    }
});