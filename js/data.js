define(function() {
    return {
        getPageContentHierarchy: function(pageId, callback) {
        },

        getSpaceHierarchy: function(spaceKey, callback) {
        },

        removePage: function(pageId, callback) {
        },

        movePage: function(pageId, newParentId, callback) {
        },

        movePageToTopLevel: function(pageId, spaceKey, callback) {
        }
    }
});