require(['config'], function() {
    require(["ap-loader"], function(apLoader) {
        apLoader.load(function() {
            AP.require(['dialog', 'events'], function(dialog, events){
                dialog.getButton("submit").bind(function() {
                    events.emit("confirmPageRemoval");
                    dialog.close();
                });
            });
        });
    });
});