require(["config"], function() {
    require(["ap-loader", "graph"], function(apLoader, graph) {
        apLoader.load(function() {
            $(window).one("resize", function() {
                graph.init();
            });
        });
    });
});