define(["jquery", "host"], function($, host) {
    var script = document.createElement("script");

    $(function () {
        script.src = host.baseUrl + '/atlassian-connect/all-debug.js';
        script.setAttribute('data-options', "sizeToParent:true");
        document.getElementsByTagName("head")[0].appendChild(script);
    });

    return $(script);
});