define([
    "jquery",
    "host",
    "purl",
    "vis",
    "data",
    "removePageDialog",
    "tooltips",
    "message"
], function($,
            host,
            purl,
            vis,
            data,
            removePageDialog,
            tooltips,
            message) {

    var spaceKey = purl().param("spaceKey");

    function getSpaceNodeId() {
        return 0;
    }

    var nodes = [
        {id: getSpaceNodeId(), group: "space"}
    ];

    var edges = [
    ];

    var graph;

    function init() {
        var container = document.getElementById('space-graph');
        var nodeData = {
            nodes: nodes,
            edges: edges
        };

        var graphWidth = $(".space-graph").width();
        var graphHeight = $(window).height() - $("#sliding-content form.aui").outerHeight();

        graph = new vis.Network(container, nodeData, {
            width: graphWidth + 'px',
            height: graphHeight + 'px',
            smoothCurves: false,
            physics: {
                hierarchicalRepulsion: {
                    nodeDistance: 175,
                    springLength: 250,
                    springConstant: 0.231,
                    damping: 0.21
                }
            },
            hierarchicalLayout: {
                levelSeparation: 220,
                nodeSpacing: 300
            },
            color: {
                highlight: "#FFFFFF"
            },
            groups: {
                space: {
                    shape: "box",
                    color: {
                        background: "#205081",
                        border: "#3572b0"
                    },
                    fontColor: "#FFFFFF"
                },
                page: {
                    shape: "box",
                    color: {
                        background: "#e9e9e9",
                        border: "#3572b0"
                    },
                    fontColor: "#3572b0"
                },
                collapsed: {
                    shape: "box",
                    color: {
                        background: "#3572b0",
                        border: "#3572b0"
                    },
                    fontColor: "#ebf2f9"
                },
                expanded: {
                    shape: "box",
                    color: {
                        background: "#3572b0",
                        border: "#3572b0"
                    },
                    fontColor: "#ebf2f9"
                }
            }
        });

        graph.on('click', function (selected) {
            var selectedNodes = idsToNodes(selected.nodes);

            if (selectedNodes.length === 1) {
                var node = selectedNodes[0];
                if (node.group === "collapsed") {
                    explodeParent(node);
                }
            }

            if (customClickHandler) {
                customClickHandler(selectedNodes);
            }
        });

        $(document).on("click", ".delete-page", function(e) {
            e.preventDefault();
            var id = $(e.target).closest("[data-node-id]").data("node-id");
            removePageDialog(function() {
                deletePage(id);
            });
        });

        function selectNode(node) {
            var pageNode = graph.nodesData.get(node);
            if (pageNode.id !== 0) {
                var $pageDetails = $(".page-details");
                $pageDetails.html(tooltips.getPopupHtml(pageNode));
                AJS.InlineDialog($pageDetails.find(".move-page"), 1,
                    function(content, trigger, showPopup) {
                        movePage($(trigger).closest(".space-graph-tooltip").data("node-id"));
                        content.css({"padding":"20px"}).html('<h3>Move page</h3><p>Click on the new parent page to the left.</p>');
                        showPopup();
                        return false;
                    }
                );

                openNav();
            } else {
                clearClickHandler();
                closeNav();
            }
        }

        function deletePage(id) {
            data.removePage(id, function (response) {
                if (JSON.parse(response) === true) {
                    reparentChildren(id, getSpaceNodeId());
                } else {
                    message.show("You can't delete that page.", 2000);
                }
            });
        }

        function openNav() {
            $("nav").addClass("nav-open");
        }

        function closeNav() {
            $("nav").removeClass("nav-open");
        }

        graph.on('select', function (selected) {
            if (selected.nodes.length) {
                selectNode(selected.nodes[0]);
            } else {
                closeNav();
            }
        });

        $("#sliding-content form.aui").click(function() {
            clearSelection();
            closeNav();
        });

        if (spaceKey) {
            data.getSpaceHierarchy(spaceKey, function(response) {
                crawlSpace(JSON.parse(response));
            });
        } else {
            // do 404
            var circleCount = 100;
            for (var i = 1; i <= circleCount; i++) {
                graph.nodesData.add({id: i, label: " "});
            }
            for (var i = 1; i <= circleCount; i++) {
                graph.edgesData.add({from: i, to: (i % circleCount) + 1});
            }
            graph.nodesData.remove(0);
            $("#mode-select-container").hide();
            message.show("There's no space here, or you're not allowed to see it.");
        }
    }

    function daysSince(dateString) {
        var nowMs = new Date().getTime();
        var msSince = nowMs - dateFromString(dateString).getTime();
        return Math.floor(msSince / (1000 * 60 * 60 * 24));
    }

    // adapted from http://stackoverflow.com/a/9413229/2484180
    function dateFromString(dateString) {
        var a = $.map(dateString.split(/[^0-9]/), function (s) {
            return parseInt(s, 10)
        });
        return new Date(a[0], a[1] - 1 || 0, a[2] || 1, a[3] || 0, a[4] || 0, a[5] || 0, a[6] || 0);
    }

    var maxModeValues = {
        daysSinceUpdated: 0,
        daysSinceCreated: 0,
        attachments: 0,
        comments: 0,
        depth: 0
    };

    function generateNode(page, parent) {
        var daysSinceUpdated = daysSince(page.lastModifiedDate.date);
        var daysSinceCreated = daysSince(page.createdDate.date);
        var depth = parent.depth + 1;
        var attachmentSize = page.attachments ? page.attachments.size : 0;
        var commentSize = page.comments ? page.comments.total : 0;

        maxModeValues.daysSinceUpdated = Math.max(maxModeValues.daysSinceUpdated, daysSinceUpdated);
        maxModeValues.daysSinceCreated = Math.max(maxModeValues.daysSinceCreated, daysSinceUpdated);
        maxModeValues.attachments = Math.max(maxModeValues.attachments, attachmentSize);
        maxModeValues.comments = Math.max(maxModeValues.comments, commentSize);
        maxModeValues.depth = Math.max(maxModeValues.depth, depth);

        var children = page.children ? page.children.content : [];

        function splitOnWord(title) {
            function b(memo, value, i) {
                var join = "";
                if (i > 0) {
                    join = (i % 2 === 0) ? "\n" : " ";
                }
                return memo + join + value
            }
            var result = _.reduce(title.split(" "), b, "");

            return _.reduce(result.split("\n"), function(memo, value) {
                if (value.length >= 20) {
                    value = value.split(" ").join("\n");
                }

                return memo + "\n" + value;
            });
        }

        return {
            id: page.id,
            parentId: parent.id,
            label: splitOnWord(page.title),
            group: children.length > 0 ? "page" : "collapsed",
            daysSinceUpdated: daysSinceUpdated,
            updatedBy: page.lastModifier,
            daysSinceCreated: daysSinceCreated,
            createdBy: page.creator,
            attachments: attachmentSize,
            comments: commentSize,
            depth: depth,
            children: children.length,
            childrenIds: _.map(children, function(child) {
                return child.id;
            })
        };
    }

    function clearSelection() {
        graph.selectNodes([]);
    }

    function deselect(pageId) {
        var selected = graph.getSelection().nodes;
        var index = selected.indexOf(pageId + "");
        if (index > -1) {
            selected.splice(index, 1);
        }
        graph.selectNodes(selected);
    }

    var customClickHandler;

    function setClickHandler(fn) {
        customClickHandler = fn;
    }

    function clearClickHandler() {
        customClickHandler = undefined;
    }

    function reparent(childId, newParentId) {
        var parent = graph.nodesData.get(newParentId);
        var child = graph.nodesData.get(childId);

        // remove edge to old parent
        graph.edgesData.remove(child.edgesToParent);
        graph.nodesData.remove(childId);
        graph.nodesData.update(parent);

        parent.childrenIds.push(childId);

        explodeParent(parent);
    }

    function reparentChildren(oldParentId, newParentId) {
        var oldParent = graph.nodesData.get(oldParentId);
        if (oldParent.group === "collapsed") {
            explodeParent(oldParent);
        }

        var children = graph.nodesData.get({
            filter: function (item) {
                return item.edgeToParent && graph.edgesData.get(item.edgeToParent).from === oldParentId;
            }
        });

        for (var i = 0; i < children.length; i++) {
            var child = children[i];
            var edge = graph.edgesData.get(child.edgesToParent);
            edge.from = newParentId;
            graph.edgesData.update(edge);
        }
        graph.nodesData.remove(oldParentId);
    }

    function idsToNodes(ids) {
        var nodes = [];
        for (var i = 0; i < ids.length; i++) {
            nodes.push(graph.nodesData.get(ids[0]));
        }
        return nodes;
    }

    function crawlSpace(space) {
        graph.nodesData.update({
            id: getSpaceNodeId(),
            label: space.name,
            group: "space",
            depth: 0
        });

        var spaceNode = graph.nodesData.get(getSpaceNodeId());

        _.each(space.rootpages.content, function(rootPage) {
            data.getPageContentHierarchy(rootPage.id, function(response) {
                addPage(JSON.parse(response), spaceNode);
            });
        });
    }

    function addPage(childPage, parent) {
        var nodeData = generateNode(childPage, parent);
        var edge = graph.edgesData.add({from: parent.id, to: childPage.id});
        nodeData.group = (nodeData.children === 0) ? "page" : "collapsed";
        nodeData.edgesToParent = edge[0];
        graph.nodesData.add(nodeData);
    }

    function explodeParent(parent) {
        _.each(parent.childrenIds, function(childPageId) {
            data.getPageContentHierarchy(childPageId, function(response) {
                var childPage = JSON.parse(response);

                addPage(childPage, parent);

                if (parent.group !== "space") {
                    parent.group = "expanded";
                }

                graph.nodesData.update(parent);
            });
        });
    }

    function movePage(id) {

        var pageToMove = {
            label: "",
            id: id
        };

        setClickHandler(function (selectedNodes) {
            // make sure we've selected something
            if (selectedNodes.length === 0 || selectedNodes[0].id === pageToMove.id) {
                return;
            }

            // don't allow selection of multiple parents - clear the selection
            if (selectedNodes.length > 1) {
                clearSelection();
                return;
            }

            var newParent = selectedNodes[0];

            if (newParent.id === getSpaceNodeId()) {
                // reparent to space
                data.movePageToTopLevel(pageToMove.id, getSpaceNodeId(), function (response) {
                    if (JSON.parse(response) === true) {
                        console.log("Moved " + pageToMove.id + " to top level.");
                        reparent(pageToMove.id, getSpaceNodeId());
                    } else {
                        console.error("Failed to move " + pageToMove.id + " to top level!");
                        message.show("You can't move that page.", 2000);
                    }
                });
            } else {
                // reparent to another page
                data.movePage(pageToMove.id, newParent.id, function (response) {
                    if (JSON.parse(response) === true) {
                        console.log("Moved " + pageToMove.id + " to " + newParent.id);
                        reparent(pageToMove.id, newParent.id);
                    } else {
                        console.error("Failed to move " + pageToMove.id + " to " + newParent.id + "!");
                        message.show("You can't move that page.", 2000);
                    }
                });
            }

            clearClickHandler();
            clearSelection();
        });
    }

    return {
        init: init,
        reparentChildren: reparentChildren,
        clearClickHandler: clearClickHandler,
        getSpaceNodeId: getSpaceNodeId
    };
});