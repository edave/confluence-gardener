define(['jquery'], function($) {
    var $message = $("#space-graph-message");
    $message.hide(); // start hidden

    function show(message, timeout, html) {
        $message[html ? "html" : "text"](message).stop().show();
        if (timeout) {
            setTimeout(hide, timeout);
        }
        $message.css("left", (($(window).width() - $message.width()) / 2) + "px"); // TODO there must be a way to do this with CSS
    }

    function hide() {
        $message.fadeOut();
    }

    function on(event, selector, handler) {
        $message.on(event, selector, handler);
    }

    return {
        show: show,
        hide: hide,
        on: on
    }
});