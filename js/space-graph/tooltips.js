define(['lodash', 'jquery', 'host'], function(_, $, host) {

    var enabled = true;
    var template = _.template($("#space-graph-tooltip-template").html());

    function formatDays(days) {
        return days + (days === 1 ? " day" : " days") + " ago";
    }

    function getPopupHtml(pageNode) {
        if (!enabled) {
            return undefined;
        }

        var data = _.extend({}, pageNode, host, {
            daysSinceUpdated: formatDays(pageNode.daysSinceUpdated),
            daysSinceCreated: formatDays(pageNode.daysSinceCreated),
            updatedBy: pageNode.updatedBy,
            createdBy: pageNode.createdBy
        });

        return template(data);
    }

    function isEnabled() {
        return enabled;
    }

    function enable() {
        enabled = true;
    }

    function disable() {
        enabled = false;
    }

    function remove() {
        $(".space-graph-tooltip").remove();
    }

    return {
        getPopupHtml: getPopupHtml,
        isEnabled: isEnabled,
        enable: enable,
        disable: disable,
        remove: remove
    };
});